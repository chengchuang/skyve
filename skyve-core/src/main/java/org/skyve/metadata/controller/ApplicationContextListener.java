package org.skyve.metadata.controller;

public interface ApplicationContextListener {
	void startup();
	void shutdown();
}
